# Coffee Bar Project

Analysis of data from a coffee bar.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Pycharm 
- Python 3.x
- Libraries : numpy, pandas, datetime, matplotlib


### Installing

1) The project can be cloned from bitbucket at this url : 
2) Import the project from your file system in Pycharm
3) Run different files for testing


End with an example of getting some data out of the system or using it for a little demo


## Built With

* [Pycharm](https://www.jetbrains.com/pycharm/) - IDE
* [Python 3.x](https://www.python.org/download/releases/3.0/) - Programming Language


## Versioning

We use [GitHub](https://git-scm.com/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/irinavos/examunamur2018/src/master/). 

## Authors

* **Irina Vostrova** - *Initial work*

## License

No license for this projet.
