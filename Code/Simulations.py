from Code.Customer import *
from random import randint
from datetime import datetime
import csv
import matplotlib.pyplot as plt

def generateReturning():
    pool = []
    for i in range(0, 1000):
        if i in (0, 333):
            pool.append(RegularHipsters())
        else:
            pool.append(RegularRegular())
    return pool

def getCust(pool):
    regOne = randint(1, 10)
    if regOne <= 2 and len(pool) > 0:
        regTemp = random.choice(pool)
        return regTemp
    else:
        oneTime = randint(1, 10)
        if oneTime <= 1:
            return OnesTripadvisor()
        else:
            return OnesRegular()

# Main program
Tab_drink = pd.read_csv('tab_drink.csv', sep=';')
Tab_food = pd.read_csv('tab_food.csv', sep=';')

pool = generateReturning()

# Extract date column
datacoffee = pd.read_csv('Coffeebar_2013-2017.csv', sep=';')
timecol = pd.to_datetime(datacoffee["TIME"])

columns = ['TIME', 'CustID', 'Drink', 'DPrice', 'Food', 'FPrice', 'Budget', 'Type']

with open('data_simulationbis.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=columns, delimiter=';')
    writer.writeheader()
    dic = []
    for i in timecol:
        cust = getCust(pool)
        cust.Buy(str(i.time()), Tab_drink, Tab_food)
        dicUst = {'TIME': datetime.strftime(i, '%d/%m/%Y %H:%M'), 'CustID': str(cust.id), 'Drink': cust.last_drink,
                  'DPrice': cust.last_drink_price, 'Food': cust.last_food, 'FPrice': cust.last_food_price,
                  'Budget': cust.budget, 'Type': cust.type}
        writer.writerow(dicUst)

        # Part 4 - Print historique ( start only if you want to see the buying history of returning customers)
        if (cust.type == "RegularRegular" or cust.type == "Hipsters"):
            cust.printHistorique()

        # delete customer with <10 budget from pool
        if (cust.type == "RegularRegular" or cust.type == "Hipsters") and cust.budget < 10:
            pool.remove(cust)

data_simulation = pd.read_csv('data_simulation.csv', sep=';')

# Bar plot 1: Graph of the total amount of different kind of food
liste_food = data_simulation['Food'].groupby(data_simulation["Food"]).count()
a = liste_food.plot(kind='bar', title=' Graph of the total amount of different kind of food', figsize=(15, 10),
                    legend=True)
a.set_xlabel("Kind of food", fontsize=8)
a.set_ylabel("Total amount", fontsize=12)
plt.figure()

# BAR PLOT 2 : Graph of the total amount of different kind of drinks
liste_drink = data_simulation['Drink'].groupby(data_simulation["Drink"]).count()
b = liste_drink.plot(kind='bar', title='Graph of the total amount of different kind of drinks', figsize=(15, 10),
                     legend=True)
b.set_xlabel("Kind of drinks", fontsize=8)
b.set_ylabel("Number", fontsize=12)
plt.figure()

