import pandas as pd
from Code.Customer import *
from random import randint
from datetime import datetime
import matplotlib.pyplot as plt

## Show some buying histories of returning customers for simulations ##

# For this question, I excecuted in part "simulation"
# Part 4 - Print historique
# if(cust.type == "RegularRegular" or cust.type == "Hipsters"):
#  cust.printHistorique()

## How many returning customers ? ##
df = pd.read_csv('Coffeebar_2013-2017.csv', sep=';')

liste = df['CUSTOMER'].groupby(df['CUSTOMER']).size()
print(liste)
liste2 = list(liste)
liste3 = list([])
for i in liste2:
    if i != 1:
        liste3.append(i)
number_returning_customer = len(liste3)
print('There are %s returnings customers ' % (str(number_returning_customer)))

## Do they have specific times when they show up more ? ##

mask = df['CUSTOMER'].value_counts()[df['CUSTOMER'].value_counts() > 1]
maskList = mask.index
return_df = df[df['CUSTOMER'].isin(maskList)]
time = pd.to_datetime(return_df["TIME"]).dt.time
var = return_df.groupby([time]).count()
var = var.sort_values(['CUSTOMER'], ascending=False)
print(var)

## Can you determine a probability of having a onetime or returning customer at a given time ##

df1 = df.assign(TYPE='rien')
df1.set_index('CUSTOMER', drop=False)
for index, row in df1.iterrows():
    if row['CUSTOMER'] in maskList:
        row['TYPE'] = 'returning'
    else:
        row['TYPE'] = "onetime"
print(df1)

Temps = pd.to_datetime(df1["TIME"]).dt.time
Tab = df1.groupby([Temps, 'TYPE']).size().unstack()
Tab1 = Tab.div(Tab.sum(1), axis=0)
print(Tab1)

# What would happen if we lower the returning customers to 50 and simulate the same period?
# I change 1000 by 50 in generateReturning()in part 3 and I restart the simulation with these new data:

def generateReturning():
    pool = []
    for i in range(0, 50):
        if i in (0, 333):
            pool.append(RegularHipsters())
        else:
            pool.append(RegularRegular())
    return pool


#The prices change from the beginning of 2015 and go up by 20%

def givePrice2(product, time):

    year = datetime.strptime(time, '%d/%m/%Y %H:%M').year()

    price = {
        'cookie': 2,
        'sandwich': 5,
        'milkshake': 5,
        'frappucino': 4,
        'water': 3,
        'rien': 0
    }.get(product, 3)

    if year >= 2015:
        return price * 1.2
    else:
        return price


# The budget of hipsters drops to 40
# I change the bugdet into 40 in def_init of the class RegularHipsters from the part Customer

class RegularHipsters(Regular):
    def __init__(self):
        Regular.__init__(self, 40)
        self.type = "Hipsters"


## TWO things I would like to see the impact of ##

# THE FIRST

# Bar plot of different type of customer from data_simulation
data_simulation_hipsters_40 = pd.read_csv('data_simulation_hipsters_40.csv', sep=';')
liste1 = data_simulation_hipsters_40['Type'].groupby(data_simulation_hipsters_40["Type"]).count()
b = liste1.plot(kind='bar', title='Graph of different type of customers when hipsters have budget of 40 ',
                figsize=(15, 10), legend=True)
b.set_xlabel("Type of customers", fontsize=8)
b.set_ylabel("The amount", fontsize=12)
plt.figure()

# Bar plot of different type of customer from data_simulation_hipsters_40
data_simulation = pd.read_csv('data_simulation.csv', sep=';')
liste2 = data_simulation['Type'].groupby(data_simulation["Type"]).count()
b = liste2.plot(kind='bar', title='Graph of different type of customers when hipsters have budget of 500',
                figsize=(15, 10), legend=True)
b.set_xlabel("Type of customers", fontsize=8)
b.set_ylabel("The amount", fontsize=12)
plt.figure()

data_simulation_price_20 = pd.read_csv('data_simulation_price_20.csv', sep=';')

# THE SECOND

# BAR PLOT : Graph of the total amount of different kind of drinks when the price
liste_drink = data_simulation_price_20['Drink'].groupby(data_simulation_price_20["Drink"]).count()
b = liste_drink.plot(kind='bar',
                     title='Graph of the total amount of different kind of drinks after the price increase of 20%',
                     figsize=(15, 10), legend=True)
b.set_xlabel("Kind of drinks", fontsize=8)
b.set_ylabel("Number", fontsize=12)
plt.figure()
