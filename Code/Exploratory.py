import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data_coffee = pd.read_csv('coffeebar_2013-2017.csv', sep=';')

temps = pd.to_datetime(data_coffee["TIME"])
print(temps)

# What kind of food ?
kind_food = data_coffee.FOOD.unique()
kind_food = [i for i in kind_food if str(i) != 'nan']
print('Food sold by coffee bar are : %s ' % (kind_food))

# What kind of drinks ?
kind_drinks = data_coffee.DRINKS.unique()
print('Drinks sold by coffee bar are : %s' % (kind_drinks))

# How many unique customers did the bar have ?
number = len(data_coffee.CUSTOMER.unique())
print('The bar has ' + str(number) + ' unique customers')

# BAR PLOT 1 : Graph of the total amount of different kind of food
data_coffee1 = data_coffee.replace(np.nan, 'rien')
liste_food = data_coffee1['FOOD'].groupby(data_coffee1["FOOD"]).count()
a = liste_food.plot(kind='bar', title=' Graph of the total amount of different kind of food', figsize=(15, 10),
                    legend=True)
a.set_xlabel("Kind of food", fontsize=8)
a.set_ylabel("Total amount", fontsize=12)
plt.figure()

# BAR PLOT 2 : Graph of the total amount of different kind of drinks
liste_boisson = data_coffee['DRINKS'].groupby(data_coffee["DRINKS"]).count()
b = liste_boisson.plot(kind='bar', title='Graph of the total amount of different kind of drinks', figsize=(15, 10),
                       legend=True)
b.set_xlabel("Kind of drinks", fontsize=8)
b.set_ylabel("Number", fontsize=12)
plt.figure()

# BAR PLOT 1 :  A bar plot of the amount of  DRINKS and FOOD for each year
data_coffee['TIME'] = pd.to_datetime(data_coffee['TIME'])
y2013 = (data_coffee[data_coffee['TIME'].dt.year == 2013]).count()
y2014 = (data_coffee[data_coffee['TIME'].dt.year == 2014]).count()
y2015 = (data_coffee[data_coffee['TIME'].dt.year == 2015]).count()
y2016 = (data_coffee[data_coffee['TIME'].dt.year == 2016]).count()
y2017 = (data_coffee[data_coffee['TIME'].dt.year == 2017]).count()

Y_Food = [y2013['FOOD'], y2014['FOOD'], y2015['FOOD'], y2016['FOOD'], y2017['FOOD']]
Y_Drink = [y2013['DRINKS'], y2014['DRINKS'], y2015['DRINKS'], y2016['DRINKS'], y2017['DRINKS']]

# Create a dataframe
d = {'year': ['2013', '2014', '2015', '2016', '2017'], 'Food': Y_Food, 'Drinks': Y_Drink}
df = pd.DataFrame(data=d)
df = df.set_index('year', drop=False)

# Show the bar plot
ax = df[['Food', 'Drinks']].plot(kind='bar', title='The total amount of food and drink for each year', figsize=(15, 10),
                                 legend=True)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("The total amount", fontsize=12)
plt.figure()

# Determinate the average that a customer buys a certain food or drinks at any given time

# DRINKS
Temps = pd.to_datetime(data_coffee1["TIME"]).dt.time
Prob_drink = data_coffee1.groupby([Temps, 'DRINKS']).size().unstack()
Prob_drink1 = Prob_drink.div(Prob_drink.sum(1), axis=0)
print(Prob_drink1)

# FOOD
Proba_Food = data_coffee1.groupby([Temps, "FOOD"]).size().unstack()
Proba_food1 = Proba_Food.div(Proba_Food.sum(1), axis=0)
Proba_food1 = Proba_food1.replace(np.nan, 0)
print(Proba_food1)

# Save the table of probability to use it in PART 2 :

# File for Drink probability
Prob_drink1.to_csv('tab_drink.csv', sep=';', float_format='%.4f')

# File for Food probability
Proba_food1.to_csv('tab_food.csv', sep=';', float_format='%.4f')
