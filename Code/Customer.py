import uuid
from random import randint
import pandas as pd
import random

def random_pick(liste_produit, proba):
    x = random.uniform(0, 1)
    cumulative_probability = 0.0
    for item, item_probability in zip(liste_produit, proba):
        cumulative_probability += item_probability
        if x < cumulative_probability: break
    return item

def commandeAtTime(Tab_produit, time):
    Tab_produit1 = Tab_produit.set_index("TIME", drop=False)
    index = Tab_produit1.loc[time, :]
    index = index.drop("TIME")

    liste_produit = Tab_produit.drop('TIME', axis=1)
    liste_produit = liste_produit.columns.values

    proba = index.values.tolist()
    return random_pick(liste_produit, proba)

class Customer(object):
    def __init__(self, budget):
        self.budget = budget
        self.id = uuid.uuid4()

    # Méthode acheter
    def Buy(self, time, Tab_drink, Tab_food):
        self.last_drink = commandeAtTime(Tab_drink, time)
        self.last_food = commandeAtTime(Tab_food, time)
        self.last_food_price = givePrice(self.last_food)
        self.last_drink_price = givePrice(self.last_drink)
        self.budget = self.budget - self.last_food_price - self.last_drink_price

    def printCust(self):
        print("Le client a acheté : %s à %s euros " % (self.last_drink, str(self.last_drink_price)))
        print("Le client a acheté : %s à %s euros " % (self.last_food, str(self.last_food_price)))
        print("L'id du client est :  %s  " % (self.id))
        print("Le budget du client est de %s euros" % (self.budget))

class OnesCustomer(Customer):
    def __init__(self):
        Customer.__init__(self, 100)

class OnesRegular(OnesCustomer):
    def __init__(self):
        OnesCustomer.__init__(self)
        self.type = "OnesRegular"


class OnesTripadvisor(OnesCustomer):
    def __init__(self):
        OnesCustomer.__init__(self)
        self.type = "Tripadvisor"

    def Buy(self, time, Tab_drink, Tab_food):
        OnesRegular.Buy(self, time, Tab_drink, Tab_food)
        self.last_pourboire = randint(1, 10)
        self.budget = self.budget - self.last_pourboire

    def printCust(self):
        OnesRegular.printCust(self)
        print("Pourboire : %s" % (self.last_pourboire))

class Regular(Customer):

    def __init__(self, budget):
        Customer.__init__(self, budget)
        columns = ['Drink', 'DrinkPrice', 'Food', 'FoodPrice', 'Budget']
        self.historique = pd.DataFrame(columns=columns)
        self.historique = self.historique.fillna(0)

    def Buy(self, time, Tab_drink, Tab_food):
        Customer.Buy(self, time, Tab_drink, Tab_food)
        self.historique.loc[time] = [self.last_drink, self.last_drink_price, self.last_food, self.last_food_price,
                                     self.budget]

    def printHistorique(self):
        print(self.historique)

class RegularRegular(Regular):
    def __init__(self):
        Regular.__init__(self, 250)
        self.type = "RegularRegular"

class RegularHipsters(Regular):
    def __init__(self):
        Regular.__init__(self, 500)
        self.type = "Hipsters"

def givePrice(product):
    return {
        'cookie': 2,
        'sandwich': 5,
        'milkshake': 5,
        'frappucino': 4,
        'water': 3,
        'rien': 0
    }.get(product, 3)

# PROGRAMME PRINCIPAL
Tab_drink = pd.read_csv('tab_drink.csv', sep=';')
Tab_food = pd.read_csv('tab_food.csv', sep=';')
